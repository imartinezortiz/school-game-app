import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { User } from 'api/models/user.model';
import { AuthenticationService } from 'api/requests-services/authentication/authentication.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) { }

    public canActivate(route: ActivatedRouteSnapshot): boolean {
        const currentUser: User = this.authenticationService.currentUserValue;
        if (currentUser) {
            return this.manageLoggedinRoutes(route, currentUser);
        }
        else {
            return this.manageLoggedoutRoutes(route);
        }
    }

    private manageLoggedinRoutes(route: ActivatedRouteSnapshot, currentUser: User): boolean {
        if (currentUser.role === 'TEACHER'){
            if (route.routeConfig.path === 'teachers') {
                return true;
            }
            else {
                this.router.navigate(['/teachers/dashboard']);
            }
        }
        else if (currentUser.role === 'STUDENT'){
            if (route.routeConfig.path === 'students') {
                return true;
            }
            else {
                this.router.navigate(['/students/dashboard']);
            }
        }
        return false;
    }

    private manageLoggedoutRoutes(route: ActivatedRouteSnapshot): boolean {
        if (route.routeConfig.path === 'public/login' || route.routeConfig.path === 'public/register') {
            return true;
        }
        console.log(route);
        console.log(route.routeConfig);
        console.log(route.routeConfig.path);
        this.router.navigate(['/public/login']);
        return false;
    }
}
