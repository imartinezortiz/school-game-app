import { ColumDef, GenericListComponent } from 'app/components/generic-list/generic-list.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ITeacher } from 'api/models/teacher.model';
import { User } from 'api/models/user.model';
import { AuthenticationService } from 'api/requests-services/authentication/authentication.service';
import { TeacherService } from 'api/requests-services/teacher/teacher.service';
import { StudentService } from 'api/requests-services/student/student.service';
import { IStudentClassroom } from 'api/models/student-classroom';
import { TemplateRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
    selector: 'app-student-classroom-request',
    templateUrl: './student-classroom-request.component.html',
    styleUrls: ['./student-classroom-request.component.css']
})
export class StudentClassroomRequestComponent implements OnInit
{
    @ViewChild('aceptedRequestsList', { static: false }) aceptedRequestsList: GenericListComponent;
    @ViewChild('pendingRequestsList', { static: false }) pendingRequestsList: GenericListComponent;
    @ViewChild('rejectedRequestsList', { static: false }) rejectedRequestsList: GenericListComponent;

    public teacher: ITeacher;
    public teacherId: string;
    public studentClassroomsForDataSourceModelAcepted: any[];
    public studentClassroomsForDataSourceModelPending: any[];
    public studentClassroomsForDataSourceModelRejected: any[];
    public studentClassrooms: IStudentClassroom[];
    public columnsDefinitions: ColumDef[];
    public studentClassroomToShow: IStudentClassroom;

    constructor(
        private studentService: StudentService,
        private authenticationService: AuthenticationService,
        private teacherService: TeacherService,
        private modalService: NgbModal
    ) {

    }

    public ngOnInit(): void {
        this.getCurrentTeacher();
    }

    public deleteStudentClassroom(id: string): void {
        this.studentService.deleteJoinClassroomRequest(id).toPromise().then(() => {
            window.location.reload();
        });
    }

    public acceptStudentClassroom(id: string): void {
        const studentClassroom: IStudentClassroom = this.studentClassrooms.find(sc => sc.id === id);
        this.studentService.acceptJoinClassroomRequest(studentClassroom).toPromise().then(() => {
            window.location.reload();
        });
    }

    public cancelStudentClassroom(id: string): void {
        const studentClassroom: IStudentClassroom = this.studentClassrooms.find(sc => sc.id === id);
        this.studentService.cancelJoinClassroomRequest(studentClassroom).toPromise().then(() => {
            window.location.reload();
        });
    }

    public getStudentJoinRequestStatusByCode(code: number): string {
        switch (code) {
            case 0: return 'Pendiente';
            case 1: return 'Aceptada';
            case 2: return 'Rechazada';
            case 3: return 'Eliminada';
            default: return '';
        }
    }

    public showStudentJoinRequestInformation(id: string, modal: TemplateRef<any>): void {
        this.studentClassroomToShow = this.studentClassrooms.find((sc: IStudentClassroom) => sc.id === id);
        this.modalService.open(modal, { centered: true, scrollable: true });
    }

    private getCurrentTeacher(): void {
        const currentUser: User = this.authenticationService.currentUserValue;
        this.teacherService.getTeacherByUserId(currentUser.id).toPromise()
            .then((teacher: ITeacher) => {
                this.teacher = teacher;
                this.teacherId = this.teacher.id;
                this.initializeList();
            });
    }

    private initializeList(): void {
        this.studentService.getStudentClasrromsByTeacherId(this.teacherId)
            .toPromise().then((studentClassrooms: IStudentClassroom[]) => {
                this.studentClassrooms = studentClassrooms;
                this.studentClassroomsForDataSourceModelAcepted = studentClassrooms.filter(c => c.studentClassroomStatusCode === 1)
                    .map(c => {
                        const cellElement: any = {
                            id: { element: c.id, viewValue: c.id },
                            name: { element: c.student.name, viewValue: c.student.name }
                        };
                        return cellElement;
                    });
                this.studentClassroomsForDataSourceModelPending = studentClassrooms.filter(c => c.studentClassroomStatusCode === 0)
                    .map(c => {
                        const cellElement: any = {
                            id: { element: c.id, viewValue: c.id },
                            name: { element: c.student.name, viewValue: c.student.name }
                        };
                        return cellElement;
                    });
                this.studentClassroomsForDataSourceModelRejected = studentClassrooms.filter(c => c.studentClassroomStatusCode === 2)
                    .map(c => {
                        const cellElement: any = {
                            id: { element: c.id, viewValue: c.id },
                            name: { element: c.student.name, viewValue: c.student.name }
                        };
                        return cellElement;
                    });

                this.columnsDefinitions = [
                    {
                        columnName: 'Id',
                        matColumnDef: 'id',
                        isVisible: false
                    },
                    {
                        columnName: 'Nombre',
                        matColumnDef: 'name',
                        isVisible: true
                    }
                ];
            });
    }
}
