import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-teachers-dashboard',
    templateUrl: './teachers-dashboard.component.html',
    styleUrls: ['./teachers-dashboard.component.css']
})
export class TeachersDashboardComponent implements OnInit
{

    constructor(private router: Router) {
    }

    public ngOnInit(): void
    {
        this.router.navigate(['/teachers/classrooms']);
    }
}
