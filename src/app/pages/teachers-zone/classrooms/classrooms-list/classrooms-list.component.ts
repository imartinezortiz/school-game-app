import { ColumDef, GenericListComponent, HeaderButton } from 'app/components/generic-list/generic-list.component';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ITeacher } from 'api/models/teacher.model';
import { User } from 'api/models/user.model';
import { AuthenticationService } from 'api/requests-services/authentication/authentication.service';
import { ClassroomService } from 'api/requests-services/classroom/classroom.service';
import { TeacherService } from 'api/requests-services/teacher/teacher.service';
import { IClassroom } from 'api/models/classroom.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Clipboard } from '@angular/cdk/clipboard';

@Component({
    selector: 'app-classrooms-list',
    templateUrl: './classrooms-list.component.html',
    styleUrls: ['./classrooms-list.component.css']
})
export class ClassroomsListComponent implements OnInit {
    @ViewChild('genericList', { static: false }) genericList: GenericListComponent;

    public headerButtons: HeaderButton[];
    public teacher: ITeacher;
    public teacherId: string;
    public classroomsForDataSourceModel: any[];
    public classrooms: IClassroom[];
    public columnsDefinitions: ColumDef[];
    public classroomToShow: IClassroom;

    constructor(
        private classroomService: ClassroomService,
        private clipboard: Clipboard,
        private authenticationService: AuthenticationService,
        private teacherService: TeacherService,
        private modalService: NgbModal,
        private router: Router
    ) {
        this.headerButtons = [
            {
                name: 'Nueva clase',
                classes: 'btn-outline-light',
                clickAction: this.createNewClassroom().bind(this)
            }
        ];
    }

    public ngOnInit(): void {
        this.getCurrentTeacher();
    }

    public createNewClassroom(): (() => void) {
        return () => this.router.navigate(['/teachers/classrooms/create']);
    }

    public deleteClassroom(id: string): void {
        this.classroomService.deleteClassroom(id).toPromise().then(() => {
            this.classroomsForDataSourceModel = this.classroomsForDataSourceModel.filter(c => c.id.element !== id);
            this.genericList.updateDataSource(this.classroomsForDataSourceModel);
        });
    }

    public editClassroom(id: string): void {
        this.router.navigate(['teachers/classrooms/' + id + '/edit']);
    }

    public showClassroomInformation(id: string, modal: TemplateRef<any>): void {
        this.classroomToShow = this.classrooms.find(c => c.id === id);
        this.modalService.open(modal, { centered: true, scrollable: true });
    }

    public copyCodeToClipboard(): void {
        this.clipboard.copy(this.classroomToShow.code);
    }

    private getCurrentTeacher(): void {
        const currentUser: User = this.authenticationService.currentUserValue;
        this.teacherService.getTeacherByUserId(currentUser.id).toPromise()
            .then((teacher: ITeacher) => {
                this.teacher = teacher;
                this.teacherId = this.teacher.id;
                this.initializeList();
            });
    }

    private initializeList(): void {
        this.classroomService.getClassroomsByTeacherId(this.teacherId)
            .toPromise().then((classrooms: IClassroom[]) => {
                this.classrooms = classrooms;
                this.classroomsForDataSourceModel = classrooms.map(c => {
                    const cellElement: any = {
                        id: { element: c.id, viewValue: c.id },
                        name: { element: c.name, viewValue: c.name },
                        schoolName: { element: c.schoolName, viewValue: c.schoolName },
                        schoolGrade: { element: c.schoolGrade, viewValue: c.schoolGrade }
                    };
                    return cellElement;
                });
                this.columnsDefinitions = [
                    {
                        columnName: 'Id',
                        matColumnDef: 'id',
                        isVisible: false
                    },
                    {
                        columnName: 'Nombre',
                        matColumnDef: 'name',
                        isVisible: true
                    },
                    {
                        columnName: 'Colegio',
                        matColumnDef: 'schoolName',
                        isVisible: true
                    },
                    {
                        columnName: 'Curso',
                        matColumnDef: 'schoolGrade',
                        isVisible: true
                    }
                ];
            });
    }
}
