import { Component } from '@angular/core';

@Component({
  selector: 'app-words-datasets',
  template: '<router-outlet></router-outlet>'
})
export class WordsDatasetsComponent {

}
