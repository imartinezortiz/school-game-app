import { Component, OnInit, ViewChild } from '@angular/core';
import { ColumDef, GenericListComponent, HeaderButton } from 'app/components/generic-list/generic-list.component';
import { WordsDatasetService } from 'api/requests-services/words-dataset/words-dataset.service';
import { IWordsDataset } from 'api/models/words-dataset.model';
import { User } from 'api/models/user.model';
import { AuthenticationService } from 'api/requests-services/authentication/authentication.service';
import { TeacherService } from 'api/requests-services/teacher/teacher.service';
import { ITeacher } from 'api/models/teacher.model';
import { Router } from '@angular/router';
import { TemplateRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-words-datasets-list',
    templateUrl: './words-datasets-list.component.html',
    styleUrls: ['./words-datasets-list.component.scss']
})
export class WordsDatasetsListComponent implements OnInit {
    @ViewChild('privateDatasetsTab', { static: false }) privateDatasetsTab: GenericListComponent;
    @ViewChild('publicDatasetsTab', { static: false }) publicDatasetsTab: GenericListComponent;
    // @ViewChild('genericList', { static: false }) genericList: GenericListComponent;

    public wordsDatasetsForDataSourceModelPrivate: any[];
    public wordsDatasetsForDataSourceModelPublic: any[];
    public wordsDatasets: IWordsDataset[];
    public columnsDefinitions: ColumDef[];

    public headerButtons: HeaderButton[];

    public wordDatasetToShow: IWordsDataset;

    private teacher: ITeacher;
    private teacherId: string;

    constructor(
        private authenticationService: AuthenticationService,
        private teacherService: TeacherService,
        private wordsDatasetService: WordsDatasetService,
        private modalService: NgbModal,
        private router: Router
    ) {
        this.headerButtons = [
            {
                name: 'Nuevo conjunto',
                classes: 'btn-outline-light',
                clickAction: this.goToCreateNewWordDataset().bind(this)
            }
        ];
    }

    public ngOnInit(): void {
        this.getCurrentTeacher();
    }

    public goToCreateNewWordDataset(): (() => void) {
        return () => this.router.navigate(['/teachers/words-datasets/create']);
    }

    public showWordDataset(id: string, modal: TemplateRef<any>): void {
        this.wordDatasetToShow = this.wordsDatasets.find(c => c.id === id);
        this.modalService.open(modal, { centered: true, scrollable: true });
    }

    public editWordDataset(id: string): void {
        this.router.navigate(['teachers/words-datasets/' + id + '/edit']);
    }

    public deleteWordDataset(id: string): void {
        this.wordsDatasetService.deleteWordsDataset(id).toPromise().then(() => {
            this.wordsDatasetsForDataSourceModelPrivate = this.wordsDatasetsForDataSourceModelPrivate
                .filter(wd => wd.id.element !== id);
            // this.genericList.updateDataSource(this.wordsDatasetsForDataSourceModelPrivate);
        });
    }

    private getCurrentTeacher(): void {
        const currentUser: User = this.authenticationService.currentUserValue;
        this.teacherService.getTeacherByUserId(currentUser.id).toPromise()
            .then((teacher: ITeacher) => {
                this.teacher = teacher;
                this.teacherId = this.teacher.id;
                this.initializeList();
            });
    }

    private initializeList(): void {
        this.wordsDatasetService.getWordsDatasetsByTeacherId(this.teacherId)
            .toPromise().then((studentClassrooms: IWordsDataset[]) => {
                this.wordsDatasets = studentClassrooms;
                this.wordsDatasetsForDataSourceModelPublic = this.parseDataForDataSourceModel(studentClassrooms.filter(wd => wd.isPublic));

                this.wordsDatasetsForDataSourceModelPrivate = this.parseDataForDataSourceModel(studentClassrooms.filter(wd => !wd.isPublic));

                this.columnsDefinitions = [
                    {
                        columnName: 'Id',
                        matColumnDef: 'id',
                        isVisible: false
                    },
                    {
                        columnName: 'Nombre',
                        matColumnDef: 'name',
                        isVisible: true
                    },
                    {
                        columnName: 'Nº de palabras',
                        matColumnDef: 'wordCount',
                        isVisible: true
                    }
                ];
            });
    }

    private parseDataForDataSourceModel(studentClassrooms: IWordsDataset[]): any[] {
        return studentClassrooms.map(wd => {
            const cellElement: any = {
                id: { element: wd.id, viewValue: wd.id },
                name: { element: wd.name, viewValue: wd.name },
                wordCount: { element: wd.words.length, viewValue: wd.words.length }
            };
            return cellElement;
        });
    }
}
