import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { Routes, RouterModule } from '@angular/router';
import { CommonComponentsModule } from 'app/components/common-components.module';
import { WordsDatasetsComponent } from './words-datasets.component';
import { WordsDatasetsFormComponent } from './words-datasets-form/words-datasets-form.component';
import { WordsDatasetsListComponent } from './words-datasets-list/words-datasets-list.component';
import { WordsDatasetsStudentAssignmentComponent } from './words-datasets-student-assignment/words-datasets-student-assignment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatRippleModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTabsModule } from '@angular/material/tabs';
import { MatChipsModule } from '@angular/material/chips';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatDividerModule } from '@angular/material/divider';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';


const routes: Routes = [
    {
        path: '',
        component: WordsDatasetsComponent,
        children: [
            {
                path: 'create',
                component: WordsDatasetsFormComponent,
                pathMatch: 'full'
            },
            {
                path: ':wordDatasetId/edit',
                component: WordsDatasetsFormComponent,
                pathMatch: 'full'
            },
            {
                path: ':wordDatasetId/student-assignment/:studentId',
                component: WordsDatasetsStudentAssignmentComponent,
                pathMatch: 'full'
            },
            {
                path: '',
                pathMatch: 'full',
                component: WordsDatasetsListComponent
            }
        ]
    },
];

@NgModule({
    declarations: [
        WordsDatasetsComponent,
        WordsDatasetsFormComponent,
        WordsDatasetsListComponent,
        WordsDatasetsStudentAssignmentComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatRippleModule,
        MatTabsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTooltipModule,
        MatChipsModule,
        MatCardModule,
        MatAutocompleteModule,
        MatGridListModule,
        MatDividerModule,
        MatCheckboxModule,
        MatRadioModule,
        CommonComponentsModule,
    ]
})
export class WordsDatasetsModule { }
