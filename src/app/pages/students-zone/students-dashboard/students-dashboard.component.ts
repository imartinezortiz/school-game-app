import { Component, OnInit } from '@angular/core';
import { IStudentStatistics } from 'api/models/student-statistics.model';
import { StudentService } from 'api/requests-services/student/student.service';
import { StudentStatisticsService } from 'api/requests-services/student-statistics/student-statistics.service';
import { IStudent } from 'api/models/student.model';
import { AuthenticationService } from 'api/requests-services/authentication/authentication.service';

@Component({
    selector: 'app-students-dashboard',
    templateUrl: './students-dashboard.component.html',
    styleUrls: ['./students-dashboard.component.css']
})
export class StudentsDashboardComponent implements OnInit
{
    public student: IStudent;
    public studentStatistics: IStudentStatistics;

    private userId: string;

    constructor(
        private studentService: StudentService,
        private studentStatisticsService: StudentStatisticsService,
        private authenticationService: AuthenticationService
    ) {
        this.userId = this.authenticationService.currentUserValue.id;
    }

    public ngOnInit(): void
    {
        this.studentService.getStudentByUserId(this.userId)
            .subscribe((student: IStudent) => this.student = student);
        this.studentStatisticsService.getStudentStatisticsByUserId(this.userId)
            .subscribe((studentStatistics: IStudentStatistics) => this.studentStatistics = studentStatistics);
    }
}
