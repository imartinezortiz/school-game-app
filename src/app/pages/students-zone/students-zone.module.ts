import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { StudentsZoneComponent } from './students-zone.component';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatRippleModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonComponentsModule } from 'app/components/common-components.module';
import { StudentsDashboardComponent } from 'app/pages/students-zone/students-dashboard/students-dashboard.component';
import { UnityComponent } from './unity/unity.component';
import { StatisticsChartsComponent } from './students-dashboard/parts/statistics-charts/statistics-charts.component';
import { StatisticsHeaderComponent } from './students-dashboard/parts/statistics-header/statistics-header.component';

const routes: Routes = [
    {
        path: '',
        component: StudentsZoneComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard',
                pathMatch: 'full'
            },
            {
                path: 'dashboard',
                component: StudentsDashboardComponent,
                pathMatch: 'full'
            },
            {
                path: 'classroom',
                children: [
                    {
                        path: '',
                        loadChildren: () => import('./student-classroom/student-classroom.module').then(m => m.StudentClassroomModule)
                    }
                ]
            },
            {
                path: 'games',
                children: [
                    {
                        path: '',
                        loadChildren: () => import('./unity/students-unity-zone.module').then(m => m.StudentsUnityZoneModule)
                    }
                ]
            },
        ]
    },
];

@NgModule({
    declarations: [
        // COMPONETS
        StudentsZoneComponent,
        StudentsDashboardComponent,
        StatisticsHeaderComponent,
        StatisticsChartsComponent,
        UnityComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatRippleModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTooltipModule,
        CommonComponentsModule,
    ]
})
export class StudentsZoneModule { }
