import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'api/requests-services/authentication/authentication.service';

declare function createUnityInstance(
    canvas: any,
    config: {
        dataUrl: string;
        frameworkUrl: string;
        codeUrl: string;
        streamingAssetsUrl: string;
        companyName: string;
        productName: string;
        productVersion: string;
        devicePixelRatio: any;
    },
    progress: (progress: any) => void
): any;

@Component({
    selector: 'app-crazy-words',
    templateUrl: './crazy-words.component.html',
    styleUrls: ['./crazy-words.component.css']
})
export class CrazyWordsComponent implements OnInit {

    public playButtonClicked = false;

    private unityInstance;
    private fullScreen = false;

    constructor(
        private authenticationService: AuthenticationService
    ) {

    }

    public onClickPlayButton(): void {
        this.playButtonClicked = true;
        if (this.unityInstance){
            this.unityInstance.SetFullscreen(1);
        }
        this.fullScreen = true;
    }

    public ngOnInit(): void {
        const buildUrl = '/assets/unity-games/crazy-words/Build';
        const loaderUrl = buildUrl + '/WebGL.loader.js';
        const config = {
            dataUrl: buildUrl + '/WebGL.data',
            frameworkUrl: buildUrl + '/WebGL.framework.js',
            codeUrl: buildUrl + '/WebGL.wasm',
            streamingAssetsUrl: 'StreamingAssets',
            companyName: 'PePaGames',
            productName: 'CrazyWords',
            productVersion: '0.1',
            devicePixelRatio: undefined,
        };

        const container = document.querySelector('#unity-container');
        const canvas: any = document.querySelector('#unity-canvas');
        const loadingBar: any = document.querySelector('#unity-loading-bar');
        const progressBarFull: any = document.querySelector('#unity-progress-bar-full');
        const fullscreenButton: any = document.querySelector('#unity-fullscreen-button');

        if (/iPhone|iPad|iPod|Android/i.test(navigator.userAgent)) {
            container.className = 'unity-mobile';
            config.devicePixelRatio = 1;
        } else {
            canvas.style.width = '800px';
            canvas.style.height = '400px';
        }
        loadingBar.style.display = 'block';

        const script = document.createElement('script');
        script.src = loaderUrl;
        script.onload = () => {
            createUnityInstance(canvas, config, (progress) => {
                progressBarFull.style.width = 100 * progress + '%';
            }).then((unityInstance) => {
                this.unityInstance = unityInstance;
                loadingBar.style.display = 'none';
                unityInstance.SendMessage(
                    'WorldManagerObject',
                    'SetCurrentUser',
                    this.authenticationService.currentUserValue.token + '{SEPARADOR}'
                    + this.authenticationService.currentUserValue.id
                );

                if (this.fullScreen) {
                    unityInstance.SetFullscreen(1);
                }

                fullscreenButton.onclick = () => {
                    unityInstance.SetFullscreen(1);
                };
            }).catch((message) => {
                alert(message);
            });
        };
        document.body.appendChild(script);
    }

}
