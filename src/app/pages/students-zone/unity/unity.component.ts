import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-unity',
    templateUrl: './unity.component.html',
    styleUrls: ['./unity.component.css']
})
export class UnityComponent implements OnInit {

    gameInstance: any;
    progress = 0;
    isReady = false;

    public gameName: string;
    public gameNameLoaded: boolean;

    constructor() { }

    public ngOnInit(): void {

    }

}
