import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatRippleModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonComponentsModule } from 'app/components/common-components.module';
import { UnityComponent } from './unity.component';
import { CrazyWordsComponent } from './crazy-words/crazy-words.component';

const routes: Routes = [
    {
        path: '',
        component: UnityComponent,
        children: [
            {
                path: 'crazy-words',
                component: CrazyWordsComponent,
                pathMatch: 'full'
            },
        ]
    },
];

@NgModule({
    declarations: [
        CrazyWordsComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatRippleModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTooltipModule,
        CommonComponentsModule,
    ]
})
export class StudentsUnityZoneModule { }
