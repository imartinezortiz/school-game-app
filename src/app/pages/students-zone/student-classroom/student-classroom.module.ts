import { CommonComponentsModule } from 'app/components/common-components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentClassroomComponent } from './student-classroom.component';
import { StudentClassroomFormComponent } from './student-classroom-form/student-classroom-form.component';
import { StudentClassroomDetailsComponent } from './student-classroom-details/student-classroom-details.component';
import { RouterModule, Routes } from '@angular/router';
import { MatTableModule } from '@angular/material/table';

const routes: Routes = [
    {
        path: '',
        component: StudentClassroomComponent,
        children: [
            {
                path: 'join',
                component: StudentClassroomFormComponent,
                pathMatch: 'full'
            },
            {
                path: '',
                component: StudentClassroomDetailsComponent,
                pathMatch: 'full'
            },
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        MatTableModule,
        CommonComponentsModule,
    ],
    declarations: [
        StudentClassroomComponent,
        StudentClassroomDetailsComponent,
        StudentClassroomFormComponent
    ]
})
export class StudentClassroomModule { }
