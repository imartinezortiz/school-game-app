import { IStudentJoinClassroomRequest } from 'api/models/student-classroom';
import { IStudent } from 'api/models/student.model';
import { AbstractFormComponent } from 'app/components/abstract-form/abstratc-form.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'api/requests-services/authentication/authentication.service';
import { User } from 'api/models/user.model';
import { StudentService } from 'api/requests-services/student/student.service';
import { EMPTY_GUID } from 'app/utils/constants';

@Component({
    selector: 'app-student-classroom-form',
    templateUrl: './student-classroom-form.component.html',
    styleUrls: ['./student-classroom-form.component.css']
})
export class StudentClassroomFormComponent extends AbstractFormComponent implements OnInit {

    private studentId: string;
    private student: IStudent;

    constructor(
        private router: Router,
        private studentService: StudentService,
        private authenticationService: AuthenticationService,
        activatedRoute: ActivatedRoute
    ) {
        super(activatedRoute);
        this.isAnEdit = false;
        this.titleBar = 'Unirse a una clase';
    }

    public ngOnInit(): void {
        this.getCurrentStudent();
    }

    public cancelFormAction(): void {
        this.router.navigate(['students/classroom']);
    }

    public submitFormAction(): void {
        if (this.formGroup.valid) {
            const controls = this.formGroup.controls;
            const joinClassroomRequest: IStudentJoinClassroomRequest = {
                message: controls.message.value,
                code: controls.code.value,
                studentId: controls.studentId.value
            };
            this.studentService.joinClassroom(joinClassroomRequest).toPromise().then(
                () => this.router.navigate(['students/classroom']),
                (error: any) => console.dir(error)
            );
        }
        else {
        }
    }

    private async initializeForm(): Promise<void> {
        this.formGroup = new FormGroup({
            id: new FormControl(EMPTY_GUID),
            code: new FormControl('', [Validators.required, Validators.minLength(15), Validators.maxLength(15)]),
            studentId: new FormControl(this.studentId),
            message: new FormControl('', Validators.maxLength(4000)),
        });
        return Promise.resolve();
    }

    private getCurrentStudent(): void {
        const currentUser: User = this.authenticationService.currentUserValue;
        this.studentService.getStudentByUserId(currentUser.id).toPromise()
            .then(async (student: IStudent) => {
                this.student = student;
                this.studentId = this.student.id;
                await this.initializeForm();
            });
    }
}
