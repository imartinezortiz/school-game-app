import { Component, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EventEmitter } from '@angular/core';

export class CellElement<T> {
    elemet: T;
    viewValue: string;
}

export class ColumDef {
    matColumnDef: string;
    columnName: string;
    isVisible: boolean;
}

export class HeaderButton {
    classes: string;
    name: string;
    clickAction: any;
}

@Component({
    selector: 'app-generic-list',
    templateUrl: './generic-list.component.html',
    styleUrls: ['./generic-list.component.css']
})
export class GenericListComponent implements OnInit {
    @Input() public listTitle: string;
    @Input() public listSubtitle: string;
    @Input() public columns: ColumDef[];
    @Input() public dataList: any[];
    @Input() public headerButtons: HeaderButton[];

    @Input() public isShowButtonVisible = true;
    @Input() public isEditButtonVisible = true;
    @Input() public isDeleteButtonVisible = true;
    @Input() public isAcceptButtonVisible = false;
    @Input() public isCancelButtonVisible = false;

    @Output() public deleteRowEvent: EventEmitter<string> = new EventEmitter<string>();
    @Output() public editRowEvent: EventEmitter<string> = new EventEmitter<string>();
    @Output() public showRowInformationEvent: EventEmitter<string> = new EventEmitter<string>();
    @Output() public acceptRowEvent: EventEmitter<string> = new EventEmitter<string>();
    @Output() public cancelRowEvent: EventEmitter<string> = new EventEmitter<string>();

    public displayedColumns: string[];
    public dataSource: MatTableDataSource<any>;

    private itemIdToDelete: string;

    constructor(private modalService: NgbModal) {
    }

    public ngOnInit(): void {
        this.displayedColumns = this.columns.filter(c => c.isVisible).map(c => c.matColumnDef);
        this.displayedColumns.push('actions');
        this.dataSource = new MatTableDataSource(this.dataList);
    }

    public cancelDialog(dialog: NgbActiveModal): void {
        dialog.dismiss();
        this.itemIdToDelete = null;
    }

    public openDialog(name: TemplateRef<any>, id: string): void {
        this.itemIdToDelete = id;
        this.modalService.open(name, { centered: true, scrollable: true });
    }

    public submitDialog(dialog: NgbActiveModal): void {
        this.deleteRowEvent.emit(this.itemIdToDelete);
        dialog.close();
        this.itemIdToDelete = null;
    }

    public updateDataSource(updatedDataList: any[]): void {
        this.dataList = updatedDataList;
        this.dataSource = new MatTableDataSource(this.dataList);
    }

    public goToEditForm(id: string): void {
        this.editRowEvent.emit(id);
    }

    public notifyAcceptEvent(id: string): void {
        this.acceptRowEvent.emit(id);
    }

    public notifyCancelEvent(id: string): void {
        this.cancelRowEvent.emit(id);
    }

    public showRowInformation(id: string): void {
        this.showRowInformationEvent.emit(id);
    }
}
