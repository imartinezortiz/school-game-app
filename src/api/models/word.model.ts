export class IWord {
    id: string;
    value: string;
    imageUrl: string;
    wordDatasetId: string;
}
