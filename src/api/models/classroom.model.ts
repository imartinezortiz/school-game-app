
export class IClassroom {
    id: string;
    name: string;
    schoolName: string;
    schoolGrade: string;
    description: string;
    code: string;
    teachersIds: string[];
}
