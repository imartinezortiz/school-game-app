export class IStudentStatistics {
    winLosseStatistics: IFinalGameStateStatistics;
    durationStatistics: IGameDurationStatistics;
}

export class IFinalGameStateStatistics {
    wins: number;
    losses: number;
    consecutiveWins: number;
    consecutiveLosses: number;
    maxConsecutiveWins: number;
    maxConsecutiveLosses: number;
    values: {[key: string]: boolean};
}

export class IGameDurationStatistics {
    min: number;
    max: number;
    avg: number;
    total: number;
    values: {[key: string]: number};
}
