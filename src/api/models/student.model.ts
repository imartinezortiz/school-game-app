export class IStudent {
    id: string;
    userId: string;
    name: string;
    description: string;
}
