import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SchoolGameApiRequestService } from 'api/requests-services/school-game-api-request.service';
import { User } from 'api/models/user.model';
import { AuthResponse } from 'api/models/auth-response.model';
import jwtDecode from 'jwt-decode';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    private baseURL: string;

    constructor(private schoolgameAPIService: SchoolGameApiRequestService<AuthResponse>) {
        this.baseURL = this.schoolgameAPIService.getApiURL();
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(sessionStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    public login(email, password): Observable<User> {
        return this.schoolgameAPIService
            .request(
                'POST',
                this.baseURL + 'authentication',
                { email, password }
            )
            .pipe(map((authResponse: AuthResponse) => {
                const user = this.getUserFromJWT(authResponse.token);
                sessionStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return user;
            }));
    }

    public logout(): void {
        // remove user from local storage and set current user to null
        sessionStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

    private getUserFromJWT(token: string): User {
        const decodedUser = jwtDecode(token) as User;
        const user: User = new User();
        user.id = decodedUser.id;
        user.role = decodedUser.role;
        user.token = token;
        return user;
    }
}
