import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SchoolGameApiRequestService } from 'api/requests-services/school-game-api-request.service';
import { IStudentStatistics } from 'api/models/student-statistics.model';
@Injectable({
  providedIn: 'root'
})
export class StudentStatisticsService {

    private baseURL: string;

    constructor(private schoolgameAPIService: SchoolGameApiRequestService<any>) {
        this.baseURL = this.schoolgameAPIService.getApiURL();
    }

    public getStudentStatisticsByUserId(userId: string): Observable<IStudentStatistics> {
        return this.schoolgameAPIService
            .request(
                'GET',
                this.baseURL + 'gameplay-events/aggregator/' + userId + '/gameplay-statistics',
                null
            );
    }
}
