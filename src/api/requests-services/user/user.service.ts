import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { SchoolGameApiRequestService } from 'api/requests-services/school-game-api-request.service';
import { User } from 'api/models/user.model';
@Injectable({
  providedIn: 'root'
})
export class UserService {

    private currentUserSubject: BehaviorSubject<User>;
    private baseURL: string;

    // private currentUser: Observable<User>;

    constructor(private schoolgameAPIService: SchoolGameApiRequestService<User>) {
        this.baseURL = this.schoolgameAPIService.getApiURL();
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(sessionStorage.getItem('currentUser')));
        // this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    public createUser(user: User): Observable<User> {
        return this.schoolgameAPIService
            .request(
                'POST',
                this.baseURL + 'users',
                JSON.stringify(user)
            );
    }
}
