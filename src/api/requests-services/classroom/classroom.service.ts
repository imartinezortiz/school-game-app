import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SchoolGameApiRequestService } from 'api/requests-services/school-game-api-request.service';
import { IClassroom } from 'api/models/classroom.model';

@Injectable({
  providedIn: 'root'
})
export class ClassroomService {
    private baseURL: string;

    constructor(
        private schoolgameAPIService: SchoolGameApiRequestService<any>
    ) {
        this.baseURL = this.schoolgameAPIService.getApiURL();
    }

    public getClassroomsByTeacherId(teacherId: string): Observable<IClassroom[]> {
        return this.schoolgameAPIService
            .request(
                'GET',
                this.baseURL + 'classrooms?teacherId=' + teacherId,
                null
            );
    }

    public getClassroom(classroomId: string): Observable<IClassroom> {
        return this.schoolgameAPIService
            .request(
                'GET',
                this.baseURL + 'classrooms/' + classroomId,
                null
            );
    }

    public createClassroom(classroom: IClassroom): Observable<IClassroom> {
        return this.schoolgameAPIService
            .request(
                'POST',
                this.baseURL + 'classrooms',
                JSON.stringify(classroom)
            );
    }

    public editClassroom(classroom: IClassroom): Observable<IClassroom> {
        return this.schoolgameAPIService
            .request(
                'PUT',
                this.baseURL + 'classrooms/' + classroom.id,
                JSON.stringify(classroom)
            );
    }

    public generateClassroomCode(): Observable<string> {
        return this.schoolgameAPIService
            .request(
                'POST',
                this.baseURL + 'classrooms/generate-code',
                null
            );
    }

    public deleteClassroom(id: string): Observable<any> {
        return this.schoolgameAPIService
            .request(
                'DELETE',
                this.baseURL + 'classrooms/' + id,
                null
            );
    }
}
