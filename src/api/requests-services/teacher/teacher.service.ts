import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SchoolGameApiRequestService } from 'api/requests-services/school-game-api-request.service';
import { ITeacher } from 'api/models/teacher.model';
import { IStudent } from 'api/models/student.model';
@Injectable({
  providedIn: 'root'
})
export class TeacherService {

    private baseURL: string;

    constructor(private schoolgameAPIService: SchoolGameApiRequestService<any>) {
        this.baseURL = this.schoolgameAPIService.getApiURL();
    }

    public getTeachers(): ITeacher[] {
        return [];
    }

    public getTeacherByUserId(userId: string): Observable<ITeacher> {
        return this.schoolgameAPIService
            .request(
                'GET',
                this.baseURL + 'teachers?userId=' + userId,
                null
            );
    }

    public getStudentsByTeacherId(teacherId: string): Observable<IStudent[]> {
        return this.schoolgameAPIService
            .request(
                'GET',
                this.baseURL + 'teachers/' + teacherId + '/students',
                null
            );
    }

    public createTeacher(teacher: ITeacher): Observable<ITeacher> {
        return this.schoolgameAPIService
            .request(
                'POST',
                this.baseURL + 'teachers',
                JSON.stringify(teacher)
            );
    }
}
