import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArasaacService {

    private baseURL: string;

    constructor(private http: HttpClient) {
        this.baseURL = 'https://api.arasaac.org/api/pictograms/es/search/';
    }

    public searchPictos(searchTerm: string): Observable<any> {
        return this.http.get(this.baseURL + searchTerm);
    }
}
