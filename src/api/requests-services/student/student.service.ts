import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SchoolGameApiRequestService } from 'api/requests-services/school-game-api-request.service';
import { IStudent } from 'api/models/student.model';
import { IStudentClassroom, IStudentJoinClassroomRequest } from 'api/models/student-classroom';
@Injectable({
  providedIn: 'root'
})
export class StudentService {

    private baseURL: string;

    constructor(private schoolgameAPIService: SchoolGameApiRequestService<any>) {
        this.baseURL = this.schoolgameAPIService.getApiURL() + 'students';
    }

    public getStudentById(studentId: string): Observable<IStudent> {
        return this.schoolgameAPIService
            .request(
                'GET',
                this.baseURL + '/' + studentId,
                null
            );
    }

    public getStudentByUserId(userId: string): Observable<IStudent> {
        return this.schoolgameAPIService
            .request(
                'GET',
                this.baseURL + '?userId=' + userId,
                null
            );
    }

    public createStudent(student: IStudent): Observable<IStudent> {
        return this.schoolgameAPIService
            .request(
                'POST',
                this.baseURL,
                JSON.stringify(student)
            );
    }

    public joinClassroom(joinClassroomRequest: IStudentJoinClassroomRequest): Observable<any> {
        return this.schoolgameAPIService
            .request(
                'POST',
                this.baseURL + '/classroom',
                JSON.stringify(joinClassroomRequest)
            );
    }

    public getStudentClasrromsByTeacherId(teacherId: string): Observable<any> {
        return this.schoolgameAPIService
            .request(
                'GET',
                this.baseURL + '/classroom?teacherId=' + teacherId,
                null
            );
    }

    public getStudentClasrromByStudentId(studentId: string): Observable<any> {
        return this.schoolgameAPIService
            .request(
                'GET',
                this.baseURL + '/classroom?studentId=' + studentId,
                null
            );
    }

    public acceptJoinClassroomRequest(studentClassroom: IStudentClassroom): Observable<void> {
        studentClassroom.studentClassroomStatusCode = 1;
        return this.schoolgameAPIService
            .request(
                'PUT',
                this.baseURL + '/classroom/' + studentClassroom.id,
                studentClassroom
            );
    }

    public cancelJoinClassroomRequest(studentClassroom: IStudentClassroom): Observable<void> {
        studentClassroom.studentClassroomStatusCode = 2;
        return this.schoolgameAPIService
            .request(
                'PUT',
                this.baseURL + '/classroom/' + studentClassroom.id,
                studentClassroom
            );
    }

    public deleteJoinClassroomRequest(studentClassroomId: string): Observable<void> {
        return this.schoolgameAPIService
            .request(
                'DELETE',
                this.baseURL + '/classroom/' + studentClassroomId,
                null
            );
    }
}
